$(document).ready(function() {
    // firstMethod
    var a = 1,
        b = 4, n=5, Xmin = 0, NewI = 0;
    var x=[],y=[];
    var E = (b-a)/(n+1);
    var N = 1+(b-a)/E;
    N = Math.round(N);
    $('.first').append('<p> N ='+N+' n = '+n+'</p>');//' E = '+E+
    var h = (b-a)/n;
    $('.first').append('<p> h ='+h+'</p>');
    for (var i = 0; i < N; i++) {
        calculateFirst();
        newInterval();
        if (i==0||i<2||i>(N-3)) {
            $('.first').append('<h2> N ='+(i+1)+'</h2>');
            outputFirst();
            if( i==(N-1)){
                $('.first').append('<hr><hr><hr><hr>');
            }}
    }
    // secondMethod
    var a = 1,
        b = 4, n=5, Xmin = 0, NewI = 0, delta = (b-a)/4, x01 = 0, x02 = 0, l2=9,e=0.0001,xRes=0, count = 0;
    while(l2>=e){
        calculateSecond();
        xRes = 0.5*(a+b);
        count++;
    }
    outputSecond();
    // third Method

    var a = 1, b = 4, x01 = a, x02=b, k=0,deltaX=0.001, x01delta = x01+deltaX, x02delta = x02+deltaX, countThird=0;
    Yx01=((((x01delta*x01delta-6*x01delta+9)-(x01*x01-6*x01+9))/deltaX));
    x=-((x01*x01-6*x01+9)/Yx01)+a;

    do{
        countThird++;
        Yx=(((((x+deltaX)*(x+deltaX)-6*(x+deltaX)+9)-(x*x-6*x+9))/deltaX))
        if (Yx<0) {
            a=x;
        }
        x=-((x*x-6*x+9)/Yx)+a;



    } while(Math.abs(Yx)>0.0001);
    $('.third').append('<h4>Метод Касательных</h4>');
    $('.third').append('<h4>Решение найдено на '+countThird+' шаге</h4>');
    $('.third').append('<p> x = '+x+'E = '+Math.abs(Yx)+'</p>');



// functions
    function calculateSecond(){
        x01 = 1/2 * (a+b-delta);
        x02 = x01+delta;
        y01 = x01*x01-6*x01+9;
        y02 = x02*x02-6*x02+9;

        if (y01<=y02) {
            b = x02;
        }
        else
        {
            a=x01;
        }
        delta = (b-a)/4;
        l2= b-a;
    }

    function calculateFirst(){
        for(var i = 0; i < n; i++){
            x[i]=a+i*h;
            y[i]=0;
            y[i] = x[i]*x[i]-6*x[i]+9;
        }
    }
    function newInterval(){
        min = y[0];
        for (var i = 0; i < n; i++) {
            if (min>y[i]) {
                min = y[i];
                NewI = i;
            }
        }
        a = x[NewI-1];
        b = x[NewI+1];
        h = (b-a)/n;
        E= (b-a)/(n+1);
    }
    function outputFirst(){
        for(var i = 0; i < n; i++){
            $('.first').append('<h4>x['+i+'] = '+x[i]+'</h4>');
            $('.first').append('<h4>y['+i+'] = '+y[i]+'</h4>');

        }
        $('.first').append('<p> minY ='+min+' E = '+E+' index = '+NewI+' minX = '+x[NewI]+'</p>');//' E = '+E+
        $('.first').append('<p> h ='+h+' a = '+a+' b = '+b+'</p>');
    }
    function outputSecond(){
        $(".second").append("<h4>Xmin = "+xRes+ " на "+count+" шаге, E = "+l2+"</h4>");
        $('.second').append('<p> a = '+a+' b = '+b+'</p>');
    }

});
