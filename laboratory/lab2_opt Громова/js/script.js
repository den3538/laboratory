$(document).ready(function () {

/*    var l1 = Math.random()-0.5;
    if (l1<0){
        var l2 = Math.random();
    }
    else {
        var l2 = Math.random()*(-1);
    }*/
//first Method
var l1=0.475;
var l2=0.6;
    var x01 = 1, x02 = 1, x1,x2,Fx, FxNew,count = 0;
    var h = 0.1, S=[l1,l2],Sm=Math.sqrt((l1*l1)+l2*l2);
    do {
    	count++;
        x1=x01+h*(S[0]/Sm);
        x2=x02+h*(S[1]/Sm);
        // Fx=Math.pow((x01*x01+x02-11),2)+Math.pow((x01+x02*x02-7),2);
        // FxNew=Math.pow((x1*x1+x2-11),2)+Math.pow((x1+x2*x2-7),2);
        Fx=4*((x01-5)*(x01-5))+((x02-6)*(x02-6));
        FxNew=4*((x1-5)*(x1-5))+((x2-6)*(x2-6));
        if(FxNew<Fx){
            x01=x1, x02=x2;
        }
        else{
            S=[-1*l1,-1*l2];
        }
     }while (Math.abs(FxNew)-Math.abs(Fx)<=0.01);
     // ouput first
    var wrapper =  $(".wrapper");
    wrapper.append("<h3>Метод случайного поиска</h3>");
    wrapper.append("x = ["+x1+";"+x2+"]");
    wrapper.append(" S = ["+l1+";"+l2+"]");
    wrapper.append("<p>Количество шагов - "+count+"</p>");

    // secondMethod
    wrapper.append("<h4>Метод коорд спуска</h4>");
    var a = -10,
        b = 10, delta = (b-a)/4, e=0.0001,xRes=0,y01,y02;
    x01 = 1, x02 = 1, l2=9, count = 0;
    while(l2>=e){
        calculateSecond1();
        xRes = 0.5*(a+b);
        count++;
    }
    outputSecond();
    a = 0,
        b = 10, Xmin = 0, delta = (b-a)/4, e=0.0001,xRes=0;
    x01 = 1, x02 = 1, l2=9, count = 0;
    while(l2>=e){
        calculateSecond2();
        xRes = 0.5*(a+b);
        count++;
    }
    outputSecond();
    // thirdMethod
    var result;
    var x0Array = [1,1], alfa=0;;
    for (var i = 0; i < 500; i++) {
    		Fx = 4*((x0Array[0]-5)*(x0Array[0]-5))+((x0Array[1]-6)*(x0Array[1]-6));
		    gradFx=[8*x0Array[0]-40,2*x0Array[1]-12];
		    Fx0grad=Math.sqrt(Math.abs(gradFx[0]*gradFx[0])+Math.abs(gradFx[1]*gradFx[1]));
		    if (Fx0grad<0.1) {
		    	result = x0Array;
		    	wrapper.append('<h3>Метод наискорейшего спуска</h3>');
		    	result[0]=result[0];
		    	result[1]=result[1];
		    	wrapper.append('<h3>x = '+result+'</h3>');
		    	return;
		    }
		    else{
//--------------------------------------------------------------------------------------------------------------
    var a = -10,
        b = 10, delta = (b-a)/4, e=0.0001,xRes=0,y01,y02;
    x01 = 1, x02 = 1, l2=9, count = 0;
    while(l2>=e){
        calculateSecond3();
        xRes = 0.5*(a+b);
        count++;
    }


// --------------------------------------------------------------------------------------------------------------
		    	alfa=xRes;
		   		    	
		    	//alfa = 2*(-1*gradFx[0])*(-1*gradFx[0])/*alfa1*/+2*(-1*gradFx[0])*x0Array[0]+2*(-1*gradFx[1])*(-1*gradFx[1])/*alfa1*/+2*(-1*gradFx[1])*x0Array[1]-16*(-1*gradFx[0])-10*(-1*gradFx[1]);
		    	x0Array=[x0Array[0]+(-alfa*gradFx[0]),(x0Array[1]+(-alfa*gradFx[1]))];
		    }
     }
    

    function calculateSecond1(){
        x01 = 1/2 * (a+b-delta);
        x02 = x01+delta;
        y01 = 4*Math.pow((x01-5),2);
        y02 = 4*Math.pow((x02-5),2);

        if (y01<=y02) {
            b = x02;
        }
        else
        {
            a=x01;
        }
        delta = (b-a)/4;
        l2= b-a;
    }

    function calculateSecond2(){
        x01 = 1/2 * (a+b-delta);
        x02 = x01+delta;
        y01 = Math.pow((x01-6),2);
        y02 = Math.pow((x02-6),2);

        if (y01<=y02) {
            b = x02;
        }
        else
        {
            a=x01;
        }
        delta = (b-a)/4;
        l2= b-a;
    }
    function outputSecond(){
        wrapper.append("<h4>Xmin = "+xRes+ " на "+count+" шаге</h4>");
    }


    function calculateSecond3(){
        x01 = 1/2 * (a+b-delta);
        x02 = x01+delta;
        
       // 4*((32.0*λ1+1.0)-5)*((32.0*λ1+1.0)-5)+((10.0*λ1+1.0)-6)*((10.0*λ1+1.0)-6)
        y01 =  4*((-gradFx[0]*x01+x0Array[0])-5)*((-gradFx[0]*x01+x0Array[0])-5)+((-gradFx[1]*x01+x0Array[1])-6)*((-gradFx[1]*x01+x0Array[1])-6);
        y02 = 4*((-gradFx[0]*x02+x0Array[0])-5)*((-gradFx[0]*x02+x0Array[0])-5)+((-gradFx[1]*x02+x0Array[1])-6)*((-gradFx[1]*x02+x0Array[1])-6);

        if (y01<=y02) {
            b = x02;
        }
        else
        {
            a=x01;
        }
        delta = (b-a)/4;
        l2= b-a;
    } 
});